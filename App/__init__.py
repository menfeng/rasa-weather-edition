from flask import Flask

from App.ext import init_app
from App.views import init_route


def create_app():
    app=Flask(__name__)

    app.config['MONGODB_SETTINGS'] = {
        'db': 'test3',
        'host': 'localhost',
        'port': 27017,
        'connect': True,
    }

    init_app(app)

    init_route(app)

    return app