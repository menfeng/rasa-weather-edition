import json
import re

import requests
from flask import Blueprint, render_template, request

blue = Blueprint('rasa', __name__)

REQUEST_URL = "http://localhost:5005/webhooks/rest/webhook"
HEADERS = {'Content-Type': 'application/json; charset=utf-8'}



def init_route(app):
    app.register_blueprint(blue)


@blue.route('/', methods=['GET', 'POST'])
def test():
    # return 'this a test page', 200
    return render_template('chat.html')


@blue.route('/chat',methods=['GET'])
def chat():
    data=request.args.to_dict()
    message=data['message']
    if message:
        answer = rasaresponse('zhongshan',message)
        return answer



def rasaresponse(user,msg):
    requestDict={'sender':user,'message':msg}
    rsp=requests.post(REQUEST_URL, data=json.dumps(requestDict),headers=HEADERS)
    # content=rsp.text.encode('utf-8').decode("unicode-escape")
    if rsp.status_code == 200:
        rsp_json=json.loads(rsp.text.encode())
        content_text = ''
        if len(rsp_json):
            # print(rsp_json)
            for i in range(len(rsp_json)):
                # txt_=rsp_json[i]['text']
                txt_=rsp_json[i].get('text')
                print('rasa返回内容是---------->{}'.format(txt_))
                if txt_:
                    content_text=content_text+txt_
                else:
                    content_text=content_text + '\n' + rsp_json[i]['image']
                # if re.findall('\{.*\}',txt_):
                #     check_json=json.loads(txt_)
                # if rsp_json[i]['text']:
                #     content_text = content_text + rsp_json['text']
                # elif rsp_json[i]['image']:
                #     content_text = content_text + '\n' + rsp_json['image']
            return content_text

        else:
            return '哎呀,这个小娜也不懂呢～～～'

    else:
        return '返回错误，请重新输入问话。'