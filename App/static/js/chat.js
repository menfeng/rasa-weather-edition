function chat(message) {
    $.ajax({
        url: "chat",
        type: "GET",
        data: {
            "message": message
        },
        success: function (data) {
            var ans = '<div class="answer">' +
                '<div class="icon left"><img src="/static/images/ai.png"/></div>' +
                '<div class="answer_text"><p id="content" style="white-space: pre-line;">' + data + '</p><i></i>' +
                '</div><div id="subText"></div><a id="btn" onclick="change()" style="color: blue;"></a></div>';

            $('.speak_box').append(ans);
            fit_screen();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("服务器出错：" + XMLHttpRequest.status);
        }
    });
}

function key_up() {
    var text = $('.chat_box input').val();
    if (text == '') {
        $('.write_list').remove();
    } else {
        var str = '<div class="write_list">' + text + '</div>';
        $('.footer').append(str);
        $('.write_list').css('bottom', $('.footer').outerHeight());
    }
}

function send_message() {
    $('.write_list').remove();
    var text = $('.chat_box input').val();
    // var text = $('.chat_box input').val();
    if (text == '') {
        alert('请输入聊天内容！');
        $('.chat_box input').focus();
        //      $('body').css('background-image', 'url(/images/bg.jpg)');
        $('body').css('background-image', 'url_for(/static/images/bg.jpg)');

    } else {
        var str = '<div class="question">' +
            '<div class="icon right"><img src="/static/images/me.png"/></div>' +
            '<div class="question_text clear">'+ text +
            '</div></div>';

        $('.speak_box').append(str);
        $('.chat_box input').val('');
        $('.chat_box input').focus();

        fit_screen();
        auto_width();
        chat(text);
    }
}

function fit_screen() {
    $('.speak_box, .speak_window').animate({
        scrollTop: $('.speak_box').height()
    }, 500);
}

function auto_width() {
    $('.question_text').css('max-width', $('.question').width() - 60);
}

// function get_data(){
//     $.getJSON('http://ds2.andunip.cn:8585/test/',function(data){
//         alert(data.data);
//         var ans = '<div class="answer">'
//                 + '<div class="icon left"><img src="images/ai.png"/></div>'
//                 + '<div class="answer_text"><p id="content" style="white-space: pre-line;">' + data.data + '</p>'
//                 + '</div></div>';

//             $('.speak_box').append(ans);
//             fit_screen();

//     });
// }

// function get_data() {
// 	$.getJSON('http://ds2.andunip.cn:8585/Hotq/', function(data) {
// 		var result = data.result
//      alert(result);
// 		var ans = '<div class="answer">' +
// 				'<div class="icon left"><img src="images/ai.png"/></div>' +
// 				'<div class="answer_text"><p id="content" style="white-space: pre-line;">' + result + '</p>' +
// 				'</div><div id="subText"></div><a id="btn" onclick="change()" style="color: blue;"></a></div>';

// 			$('.speak_box').append(ans);
// 			fit_screen();
// 	});
// }


function get_data() {
    $.ajax({
        type: "GET",
        url: "http://ds2.andunip.cn:8585/Hotq/",
        dataType: "json",
        success: function (data) {
            console.log(data);
//          alert(data.code);
//           console.log(data.data);
//           获取data里面的json数据
            var obj = data.data;
            var str = "";
//           遍历拼接获取的键值对
            for (var key in obj) {
                //str += ("<a href='#' onclick='getdata()'>"+key+":"+obj[key]+"</a> \n");
                str += ("<a href='#' onclick='getcjwt(\"" + obj[key] + "\")'>" + key + ":" + obj[key] + "</a> \n");
//               alert(key+":"+obj[key]);
            }
            console.log(str);
            var ans = '<div class="answer">'
                + '<div class="icon left"><img src="/static/images/ai.png"/></div>'
                + '<div class="answer_text"><p id="content" style="white-space: pre-line;">' + str + '</p>'
                + '</div></div>';
            $('.speak_box').append(ans);
            fit_screen();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("服务器出错：" + XMLHttpRequest.status);
        }
    });
}


// function get_data() {

//     $.ajax({
//           url:'http://ds2.andunip.cn:8585/Hotq/',
//           type:'GET',
//           dataType : "jsonp",
//           jsonp: "jsonpCallback",
//           success:function(data){ //后端返回的json数据（此处data为json对象）
//               alert(data['code']);
//           },
//           error:function () {
//               alert('异常')
//           }
//       });

// }

function getcjwt(message) {
    var text = String(message);
//   alert(text);
    var str = '<div class="question">' +
        '<div class="icon right"><img src="/static/images/me.png"/></div>' +
        '<div class="question_text clear">' + text + '<i></i>' +
        '</div></div>';

    $('.speak_box').append(str);
    $('.chat_box input').val('');
    $('.chat_box input').focus();

    fit_screen();
    auto_width();
    chat(text);
}
