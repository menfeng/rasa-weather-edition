from App.ext import db


class User(db.Document):
    # u_account=db.Column(db.String(16),unique=True)
    # u_password=db.Column(db.String(16))
    # def save(self):
    #     db.session.add(self)
    #     db.session.commit()
    u_account = db.StringField(required=True,unique=True)
    u_password = db.StringField(required=True)

    def __repr__(self):
        return 'User(u_account="{}",u_password={})'.format(self.u_account,self.u_password)