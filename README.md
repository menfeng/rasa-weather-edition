# rasa天气版

#### 介绍
rasa2.0包含查询天气和讲笑话两个功能
- 查天气属于多轮对话，讲笑话属于单轮场景。

#### 软件架构
rasa开源框架，自行了解


#### 安装教程
查看requirement.txt


#### 训练和运行
命令在shell_diectory文件夹下


#### 注意点
- 天气查询功能采用心知天气API，需要自行注册获取。你也可以更换其他天气查询API，更换成其他天气查询API时需要把天气action的逻辑变动
- 天气查询功能，地名字典需自行整理